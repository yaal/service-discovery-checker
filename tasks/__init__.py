import asyncio
import datetime
import os

import toml
from celery import Celery
from checks import dav
from checks import http
from checks import ip
from checks import smtp
from dotenv import dotenv_values
from models import db_session
from models.provider import Provider
from models.result import Result

from .utils import async_to_sync

DEFAULT_CONFIG = {
    "broker_url": "sqla+sqlite:///broker.sqlite",
    "DB_URI": "sqlite+pysqlite:///test.db",
    "broker_connection_retry_on_startup": True,
}

app = Celery(__name__)
app.conf.update(DEFAULT_CONFIG)
if os.environ.get("CONFIG"):  # pragma: no cover
    toml_config = toml.load(os.environ.get("CONFIG"))
    app.conf.update(toml_config)
app.conf.update(dotenv_values(".env"))


@app.task
@async_to_sync
async def run_checks(domain):
    with db_session(app.conf["DB_URI"]) as session:
        provider = session.query(Provider).filter(Provider.domain == domain).first()
        if not provider:
            provider = Provider(domain=domain)
            session.add(provider)
            session.commit()
        provider.last_update = datetime.datetime.utcnow()

        result_groups = await asyncio.gather(
            ip.check_ipv4(provider, domain, group="website"),
            ip.check_ipv6(provider, domain, group="website"),
            http.check_http_ipv4(provider, f"http://{domain}", group="website"),
            http.check_http_ipv4(provider, f"https://{domain}", group="website"),
            http.check_http_ipv6(provider, f"http://{domain}", group="website"),
            http.check_http_ipv6(provider, f"https://{domain}", group="website"),
            dav.check_caldav(provider, domain, group="caldav"),
            smtp.check_smtp(provider, domain, group="smtp"),
        )

        session.query(Result).filter(Result.provider_id == provider.id).delete()

        for group in result_groups:
            for result in group:
                session.add(result)

        session.commit()
        return result_groups
