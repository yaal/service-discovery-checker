from sqlalchemy import create_engine
from sqlalchemy.orm import declarative_base
from sqlalchemy.orm import Session

Base = declarative_base()


def db_engine(db_uri, init=False):
    engine = create_engine(db_uri, echo=True, future=True)
    if init:
        Base.metadata.create_all(engine)
    return engine


def db_session(db_uri=None, engine=None, init=False):
    engine = engine or db_engine(db_uri, init)
    session = Session(engine)
    return session
