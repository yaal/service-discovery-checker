from enum import Enum

from models import Base
from models.provider import Provider
from sqlalchemy import Column
from sqlalchemy import ForeignKey
from sqlalchemy import Integer
from sqlalchemy import PickleType
from sqlalchemy import String
from sqlalchemy.orm import relationship
from web.utils import HTMLValue


class Status(Enum):
    SUCCESS = "success"
    WARNING = "warning"
    INFO = "info"
    ERROR = "error"
    SKIP = "skip"
    UNKNOWN = "unknown"


class Result(Base):
    __tablename__ = "result"

    id = Column(Integer, primary_key=True)
    check_id = Column(String)
    message_id = Column(String)
    group = Column(String)
    data = Column(PickleType)

    provider_id = Column(Integer, ForeignKey("provider.id"))
    provider = relationship(Provider, back_populates="results")

    def __init__(self, **kwargs):
        for key, value in kwargs.items():
            setattr(self, key, value)

    def __repr__(self):
        return f"<Result check_id={self.check_id} message_id={self.message_id} data={self.data}>"

    @property
    def _messages(self):
        from checks.ip import MESSAGES as MESSAGES_IP
        from checks.dav import MESSAGES as MESSAGES_DAV
        from checks.http import MESSAGES as MESSAGES_HTTP
        from checks.smtp import MESSAGES as MESSAGES_SMTP

        return {
            **MESSAGES_IP,
            **MESSAGES_HTTP,
            **MESSAGES_DAV,
            **MESSAGES_SMTP,
        }

    @property
    def status(self):
        return self._messages[self.check_id][self.message_id]["status"]

    @property
    def html_data(self):
        return {
            key: value.__html__() if isinstance(value, HTMLValue) else value
            for key, value in self.data.items()
        }

    @property
    def message(self):
        return self._messages[self.check_id][self.message_id]["message"].format(
            **self.html_data
        )

    @property
    def details(self):
        return self._messages[self.check_id][self.message_id]["details"].format(
            **self.html_data
        )
