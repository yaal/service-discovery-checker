from models import Base
from sqlalchemy import Column
from sqlalchemy import DateTime
from sqlalchemy import Integer
from sqlalchemy import String
from sqlalchemy.orm import relationship


class Provider(Base):
    __tablename__ = "provider"

    id = Column(Integer, primary_key=True)
    domain = Column(String)
    last_update = Column(DateTime)
    results = relationship("Result", cascade="all, delete", back_populates="provider")
