import pytest
from checks.ip import check_ipv4
from checks.ip import check_ipv6
from web.utils import DNSRecord
from web.utils import Hyperlink


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "protocol,record_type,check,ip",
    [
        ("ipv4", "A", check_ipv4, "127.0.0.1"),
        ("ipv6", "AAAA", check_ipv6, "::1"),
    ],
)
async def test_ok(provider, testclient, dns_server, protocol, record_type, check, ip):
    result = (await check(provider, provider.domain))[0]
    assert result.check_id == "ip"
    assert result.message_id == "success"
    assert result.data == {
        "protocol": protocol,
        "ip": ip,
        "record_type": DNSRecord(record_type),
        "domain": Hyperlink(f"://{provider.domain}"),
    }


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "protocol,record_type,check,ip",
    [
        ("ipv4", "A", check_ipv4, "127.0.0.1"),
        ("ipv6", "AAAA", check_ipv6, "::1"),
    ],
)
async def test_ko(provider, testclient, dns_server, protocol, record_type, check, ip):
    result = (await check(provider, "invalid.com"))[0]
    assert result.check_id == "ip"
    assert result.message_id == "error"
    assert result.data == {
        "protocol": protocol,
        "record_type": DNSRecord(record_type),
        "domain": Hyperlink("://invalid.com"),
    }
