from models.provider import Provider
from models.result import Result


def test_everything_ok(
    file_db_session,
    file_db_testclient,
    dns_server,
    httpserver_ipv4,
    httpsserver_ipv4,
    httpserver_ipv6,
    httpsserver_ipv6,
    smtp_server_port,
    smtp_server_ipv4,
    smtp_server_ipv6,
):
    wait_message = (
        "The domain is being processed, please reload the page in a few seconds."
    )

    assert file_db_session.query(Provider.id).count() == 0
    assert file_db_session.query(Result.id).count() == 0

    res = file_db_testclient.get("/").follow()
    res.form["domain"] = "example.com"
    res = res.form.submit().follow()
    assert wait_message in res.text

    assert file_db_session.query(Provider.id).count() == 1
    assert file_db_session.query(Result.id).count() > 0

    res = file_db_testclient.get("/en/result/example.com")
    assert wait_message not in res.text
    assert "check-ip message-success status-success" in res.text
