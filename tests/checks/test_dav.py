import pytest
from checks.dav import check_caldav
from checks.dav import check_caldav_dns_records
from furl import furl
from web.utils import Code
from web.utils import DNSRecord
from web.utils import Hyperlink


@pytest.mark.asyncio
async def test_caldavs_dns_ok(
    provider,
    testclient,
    dns_server,
    httpserver_port,
    httpsserver_port,
):
    results = await check_caldav_dns_records(provider, provider.domain)

    result = results[0]
    caldavs_url = furl(
        scheme="https",
        host="caldav.example.com",
        port=httpsserver_port,
        path="/caldav/",
    )
    assert result.check_id == "caldav_dns_records"
    assert result.message_id == "success"
    assert result.data["url"] == Hyperlink(caldavs_url)
    assert result.data["caldavs_srv_record_name"] == Code("_caldavs._tcp.example.com")
    assert result.data["caldavs_srv_record_value"] == Code(
        f"0 1 {httpsserver_port} caldav.example.com"
    )
    assert result.data["caldavs_txt_record_name"] == Code("_caldavs._tcp.example.com")
    assert result.data["caldavs_txt_record_value"] == Code("path=/caldav/")


@pytest.mark.asyncio
async def test_no_dns_records(
    provider,
    testclient,
    dns_server,
    httpserver_port,
    httpsserver_port,
):
    results = await check_caldav_dns_records(provider, provider.domain)

    result = results[0]
    assert result.check_id == "caldav_dns_records"
    assert result.message_id == "error"
    assert result.data["srv_record"] == DNSRecord("SRV")
    assert result.data["caldavs_srv_record_name"] == Code("_caldavs._tcp.example.com")
    assert result.data["caldav_srv_record_name"] == Code("_caldav._tcp.example.com")


@pytest.mark.asyncio
async def test_caldavs_dns_ko_caldav_dns_ok(
    provider,
    testclient,
    dns_server,
    httpserver_port,
    httpsserver_port,
):
    results = await check_caldav_dns_records(provider, provider.domain)

    result = results[0]
    caldav_url = furl(
        scheme="http",
        host="caldav.example.com",
        port=httpserver_port,
        path="/caldav/",
    )
    assert result.check_id == "caldav_dns_records"
    assert result.message_id == "caldav_only"
    assert result.data["url"] == Hyperlink(caldav_url)
    assert result.data["caldavs_srv_record_name"] == Code("_caldavs._tcp.example.com")
    assert result.data["caldav_srv_record_name"] == Code("_caldav._tcp.example.com")
    assert result.data["caldav_txt_record_name"] == Code("_caldav._tcp.example.com")
    assert result.data["caldav_srv_record_value"] == Code(
        f"0 1 {httpserver_port} caldav.example.com"
    )
    assert result.data["caldav_txt_record_value"] == Code("path=/caldav/")


@pytest.mark.asyncio
async def test_caldavs_dns_ko_no_endpoint(
    provider,
    testclient,
    dns_server,
    httpserver_port,
    httpsserver_port,
):
    results = await check_caldav_dns_records(provider, provider.domain)

    result = results[0]
    caldav_url = furl(
        scheme="https",
        host="caldav.example.com",
        port=httpsserver_port,
    )
    assert result.check_id == "caldav_dns_records"
    assert result.message_id == "caldavs_domain_but_no_endpoint"
    assert result.data["url"] == Hyperlink(caldav_url)
    assert result.data["caldavs_srv_record_name"] == Code("_caldavs._tcp.example.com")
    assert result.data["caldavs_srv_record_value"] == Code(
        f"0 1 {httpsserver_port} caldav.example.com"
    )


@pytest.mark.asyncio
async def test_caldav_dns_ko_no_endpoint(
    provider,
    testclient,
    dns_server,
    httpserver_port,
    httpsserver_port,
):
    results = await check_caldav_dns_records(provider, provider.domain)

    result = results[0]
    caldav_url = furl(
        scheme="http",
        host="caldav.example.com",
        port=httpserver_port,
    )
    assert result.check_id == "caldav_dns_records"
    assert result.message_id == "caldav_domain_but_no_endpoint"
    assert result.data["url"] == Hyperlink(caldav_url)
    assert result.data["caldav_srv_record_name"] == Code("_caldav._tcp.example.com")
    assert result.data["caldav_srv_record_value"] == Code(
        f"0 1 {httpserver_port} caldav.example.com"
    )


@pytest.mark.asyncio
async def test_caldavs_ok(
    provider,
    testclient,
    dns_server,
    httpserver_ipv4,
    httpserver_ipv6,
    httpsserver_ipv4,
    httpsserver_ipv6,
):
    results = await check_caldav(provider, provider.domain)
    assert [(result.check_id, result.message_id) for result in results] == [
        ("caldav_dns_records", "success"),
        ("ip", "success"),
        ("ip", "success"),
        ("http_connection", "success"),
        ("http_connection", "success"),
        ("http_auth", "warning"),
    ]


@pytest.mark.asyncio
async def test_caldavs_ko_caldav_ok(
    provider,
    testclient,
    dns_server,
    httpserver_ipv4,
    httpserver_ipv6,
    httpsserver_ipv4,
    httpsserver_ipv6,
):
    results = await check_caldav(provider, provider.domain)
    assert [(result.check_id, result.message_id) for result in results] == [
        ("caldav_dns_records", "caldav_only"),
        ("ip", "success"),
        ("ip", "success"),
        ("http_connection", "success"),
        ("http_connection", "success"),
        ("http_auth", "warning"),
    ]
