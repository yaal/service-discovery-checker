import datetime
import ssl

import portpicker
import pytest
from checks.http import check_http_authentication
from checks.http import check_http_ipv4
from checks.http import check_http_ipv6
from furl import furl
from pytest_httpserver.httpserver import HTTPServer
from pytest_lazyfixture import lazy_fixture
from web.utils import AuthenticationMethod
from web.utils import Code
from web.utils import Hyperlink


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "protocol,httpserver,check",
    [
        ("IPV4", lazy_fixture("httpserver_ipv4"), check_http_ipv4),
        ("IPV6", lazy_fixture("httpserver_ipv6"), check_http_ipv6),
    ],
)
async def test_http_ok(provider, dns_server, testclient, protocol, httpserver, check):
    httpserver.expect_request("/")
    url = furl(scheme="http", host="example.com", port=httpserver.port)
    result = await check(provider, url)
    result = result[0]
    assert result.check_id == "http_connection"
    assert result.message_id == "success"
    assert result.data == {
        "http_protocol": "http",
        "ip_protocol": protocol,
        "domain": Hyperlink("://example.com"),
    }


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "protocol,check",
    [
        ("IPV4", check_http_ipv4),
        ("IPV6", check_http_ipv6),
    ],
)
async def test_http_ko_no_server(provider, dns_server, testclient, protocol, check):
    port = portpicker.pick_unused_port()
    url = furl(scheme="http", host="example.com", port=port)
    result = await check(provider, url)
    result = result[0]
    assert result.check_id == "http_connection"
    assert result.message_id == "error"
    assert result.data == {
        "http_protocol": "http",
        "ip_protocol": protocol,
        "domain": Hyperlink("://example.com"),
    }


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "protocol,httpsserver,check",
    [
        ("IPV4", lazy_fixture("httpsserver_ipv4"), check_http_ipv4),
        ("IPV6", lazy_fixture("httpsserver_ipv6"), check_http_ipv6),
    ],
)
async def test_https_ok(provider, dns_server, testclient, protocol, httpsserver, check):
    httpsserver.expect_request("/")
    url = furl(scheme="https", host="example.com", port=httpsserver.port)
    result = await check(provider, url)
    result = result[0]
    assert result.check_id == "http_connection"
    assert result.message_id == "success"
    assert result.data == {
        "http_protocol": "https",
        "ip_protocol": protocol,
        "domain": Hyperlink("://example.com"),
    }


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "protocol,check",
    [
        ("IPV4", check_http_ipv4),
        ("IPV6", check_http_ipv6),
    ],
)
async def test_https_ko_no_server(provider, dns_server, testclient, protocol, check):
    port = portpicker.pick_unused_port()
    url = furl(scheme="https", host="example.com", port=port)
    result = await check(provider, url)
    result = result[0]
    assert result.check_id == "http_connection"
    assert result.message_id == "error"
    assert result.data == {
        "http_protocol": "https",
        "ip_protocol": protocol,
        "domain": Hyperlink("://example.com"),
    }


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "protocol,httpserver,check",
    [
        ("IPV4", lazy_fixture("httpserver_ipv4"), check_http_ipv4),
        ("IPV6", lazy_fixture("httpserver_ipv6"), check_http_ipv6),
    ],
)
async def test_https_ko_only_http(
    provider, dns_server, testclient, protocol, httpserver, check
):
    url = furl(scheme="https", host="example.com", port=httpserver.port)
    result = await check(provider, url)
    result = result[0]
    assert result.check_id == "http_connection"
    assert result.message_id == "error_ssl"
    assert result.data == {
        "http_protocol": "https",
        "ip_protocol": protocol,
        "domain": Hyperlink("://example.com"),
    }


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "protocol,httpsserver,check",
    [
        ("IPV4", lazy_fixture("httpsserver_ipv4"), check_http_ipv4),
        ("IPV6", lazy_fixture("httpsserver_ipv6"), check_http_ipv6),
    ],
)
async def test_http_ko_only_https(
    provider, dns_server, testclient, protocol, httpsserver, check
):
    url = furl(scheme="http", host="example.com", port=httpsserver.port)
    result = await check(provider, url)
    result = result[0]
    assert result.check_id == "http_connection"
    assert result.message_id == "error_ssl"
    assert result.data == {
        "http_protocol": "http",
        "ip_protocol": protocol,
        "domain": Hyperlink("://example.com"),
    }


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "protocol,httpsserver,check,host",
    [
        ("IPV4", lazy_fixture("httpsserver_ipv4"), check_http_ipv4, "127.0.0.1"),
        ("IPV6", lazy_fixture("httpsserver_ipv6"), check_http_ipv6, "[::1]"),
    ],
)
async def test_https_ko_certificate_mismatch(
    provider, dns_server, testclient, protocol, httpsserver, check, host
):
    # The certificates are issued for 'example.com' but the url
    # hostname is the raw server IP. This should generate a certificate error
    url = furl(scheme="https", host=host, port=httpsserver.port)

    result = await check(provider, url)
    result = result[0]
    assert result.check_id == "http_connection"
    assert result.message_id == "error_certificate_mismatch"
    assert result.data == {
        "http_protocol": "https",
        "ip_protocol": protocol,
        "domain": Hyperlink(f"://{host}"),
    }


@pytest.mark.asyncio
async def test_https_ko_certificate_expired(provider, dns_server, testclient, ca):
    expiration_datetime = datetime.datetime(2010, 1, 1)
    context = ssl.SSLContext(ssl.PROTOCOL_TLS_SERVER)
    cert = ca.issue_cert("example.com", "*.example.com", not_after=expiration_datetime)
    cert.configure_cert(context)

    port = portpicker.pick_unused_port()
    httpsserver = HTTPServer(host="127.0.0.1", port=port, ssl_context=context)
    httpsserver.start()

    try:
        url = furl(scheme="https", host="example.com", port=httpsserver.port)
        result = await check_http_ipv4(provider, url)
        result = result[0]
        assert result.check_id == "http_connection"
        assert result.message_id == "error_certificate_expired"
        assert result.data == {
            "http_protocol": "https",
            "ip_protocol": "IPV4",
            "domain": Hyperlink("://example.com"),
        }
    finally:
        httpsserver.stop()


@pytest.mark.asyncio
async def test_http_authentication_no_header(
    provider, dns_server, testclient, httpserver_ipv4
):
    httpserver_ipv4.expect_request("/")
    result = await check_http_authentication(provider, httpserver_ipv4.url_for("/"))
    result = result[0]
    assert result.check_id == "http_auth"
    assert result.message_id == "warning"
    assert result.data == {
        "header_name": "www-authenticate",
        "header_value": "",
    }


@pytest.mark.asyncio
async def test_http_authentication_header_basic(
    provider, dns_server, testclient, httpserver_ipv4
):
    httpserver_ipv4.expect_request("/").respond_with_data(
        "Hello, World!",
        headers={"www-authenticate": 'Basic realm=whatever, charset="UTF-8"'},
    )
    result = await check_http_authentication(provider, httpserver_ipv4.url_for("/"))
    result = result[0]
    assert result.check_id == "http_auth"
    assert result.message_id == "info"
    assert result.data == {
        "header_name": "www-authenticate",
        "header_value": Code('Basic realm=whatever, charset="UTF-8"'),
        "methods": AuthenticationMethod(["basic"]),
    }


@pytest.mark.asyncio
async def test_http_authentication_header_basic_bearer(
    provider, dns_server, testclient, httpserver_ipv4
):
    httpserver_ipv4.expect_request("/").respond_with_data(
        "Hello, World!",
        headers={
            "www-authenticate": [
                'Bearer realm="Foobar"',
                'Basic realm="Foobar", charset="UTF-8"',
            ]
        },
    )
    result = await check_http_authentication(provider, httpserver_ipv4.url_for("/"))
    result = result[0]
    assert result.check_id == "http_auth"
    assert result.message_id == "info"
    assert result.data == {
        "header_name": "www-authenticate",
        "header_value": Code(
            'Bearer realm="Foobar", Basic realm="Foobar", charset="UTF-8"'
        ),
        "methods": AuthenticationMethod(["bearer", "basic"]),
    }
