import pytest
from checks.smtp import check_smtp
from web.utils import Code
from web.utils import DNSRecord
from web.utils import Hyperlink


@pytest.mark.asyncio
async def test_smtp_ok(
    provider,
    testclient,
    dns_server,
    smtp_server_port,
    smtp_server_ipv4,
    smtp_server_ipv6,
):
    results = await check_smtp(provider, provider.domain)
    assert results[0].check_id == "smtp_dns_records"
    assert results[0].message_id == "success"
    assert results[0].data == {
        "domain": Hyperlink("example.com"),
        "record_type": DNSRecord("SRV"),
        "host": "smtp.example.com",
        "port": smtp_server_port,
        "srv_record": Code("_submission._tcp.example.com"),
    }

    assert results[1].check_id == "ip"
    assert results[1].message_id == "success"
    assert results[2].check_id == "ip"
    assert results[2].message_id == "success"

    assert results[3].check_id == "smtp_connection"
    assert results[3].message_id == "success"
    assert results[3].data == {
        "host": "127.0.0.1",
        "port": smtp_server_port,
    }

    assert results[4].check_id == "smtp_connection"
    assert results[4].message_id == "success"
    assert results[4].data == {
        "host": "[::1]",
        "port": smtp_server_port,
    }


@pytest.mark.asyncio
async def test_smtp_dns_discovery_ko(
    provider, testclient, dns_server, smtp_server_ipv4, smtp_server_ipv6
):
    result = (await check_smtp(provider, provider.domain))[0]
    assert result.check_id == "smtp_dns_records"
    assert result.message_id == "error"
    assert result.data == {
        "domain": Hyperlink("example.com"),
        "record_type": DNSRecord("SRV"),
        "srv_record": Code("_submission._tcp.example.com"),
    }


@pytest.mark.asyncio
async def test_smtp_dns_domain_ko(
    provider,
    testclient,
    dns_server,
    smtp_server_port,
    smtp_server_ipv4,
    smtp_server_ipv6,
):
    results = await check_smtp(provider, provider.domain)
    assert results[1].check_id == "ip"
    assert results[1].message_id == "error"

    assert results[2].check_id == "ip"
    assert results[2].message_id == "error"


@pytest.mark.asyncio
async def test_smtp_dns_connection_ko(
    provider,
    testclient,
    dns_server,
    smtp_server_port,
    unused_port,
):
    results = await check_smtp(provider, provider.domain)
    assert results[3].check_id == "smtp_connection"
    assert results[3].message_id == "error"
    assert results[3].data == {
        "host": "127.0.0.1",
        "port": unused_port,
    }

    assert results[4].check_id == "smtp_connection"
    assert results[4].message_id == "error"
    assert results[4].data == {
        "host": "[::1]",
        "port": unused_port,
    }
