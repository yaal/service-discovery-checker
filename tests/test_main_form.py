def test_main_form_index(testclient):
    res = testclient.get("/en")
    res.form["domain"] = "yaal.coop"
    res = res.form.submit()
    assert "This is not a valid domain" not in res


def test_main_form_result(testclient):
    res = testclient.get("/en/result/yaal.fr")
    res.form["domain"] = "yaal.coop"
    res = res.form.submit()
    assert "This is not a valid domain" not in res


def test_main_form_empty(testclient):
    res = testclient.get("/en")
    res = res.form.submit()
    assert "This field is required." in res

    res = testclient.get("/en/result/yaal.fr")
    res = res.form.submit()
    assert "This field is required." in res


def test_main_form_bad_domain(testclient):
    res = testclient.get("/en")
    res.form["domain"] = "invalid"
    res = res.form.submit()
    assert "This is not a valid domain" in res


def test_unregistered_domain(testclient, mocks):
    import whois

    whois.query.return_value = None

    res = testclient.get("/en")
    res.form["domain"] = "unregistered.com"
    res = res.form.submit()
    assert "This domain is not registered" in res
