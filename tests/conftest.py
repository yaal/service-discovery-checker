import os

import pytest
from babel.messages.frontend import compile_catalog
from flask_webtest import TestApp
from web import create_app


@pytest.fixture(autouse=True, scope="session")
def babel_catalogs():
    cmd = compile_catalog()
    cmd.directory = "web/translations"
    cmd.quiet = True
    cmd.statistics = True
    cmd.finalize_options()
    cmd.run()


@pytest.fixture(autouse=True)
def mocks(mocker):
    mocker.patch("whois.query")
    return mocker


@pytest.fixture(scope="session")
def dnsserver_port():
    import portpicker

    return portpicker.pick_unused_port()


@pytest.fixture(scope="session")
def httpserver_port():
    import portpicker

    return portpicker.pick_unused_port()


@pytest.fixture(scope="session")
def httpsserver_port():
    import portpicker

    return portpicker.pick_unused_port()


@pytest.fixture(scope="session")
def smtp_server_port():
    import portpicker

    return portpicker.pick_unused_port()


@pytest.fixture()
def unused_port():
    import portpicker

    return portpicker.pick_unused_port()


@pytest.fixture(scope="session")
def ca():
    """Custom certificate authority intended to be used by the local https
    server."""
    import trustme

    return trustme.CA()


@pytest.fixture(scope="session")
def httpserver_ssl_context(ca):
    """SSL context for https server.

    This fixture is automatically used by httpserver.
    """
    import ssl

    context = ssl.SSLContext(ssl.PROTOCOL_TLS_SERVER)
    cert = ca.issue_cert("example.com", "*.example.com")
    cert.configure_cert(context)
    return context


@pytest.fixture(scope="session")
def httpclient_ssl_context(ca):
    """SSL context for clients connecting to https servers."""
    import ssl

    with ca.cert_pem.tempfile() as ca_temp_path:
        return ssl.create_default_context(cafile=ca_temp_path)


@pytest.fixture
def testclient(httpclient_ssl_context, dnsserver_port):
    """Web client to test the WSGI app."""
    TEST_CONFIG = {
        "TESTING": True,
        "SECRET_KEY": os.urandom(32),
        "DB_URI": "sqlite://",
        "CELERY_CONFIG": {
            "task_always_eager": True,
        },
        "DNS_SERVERS": ["127.0.0.1"],
        "DNS_PORT": dnsserver_port,
        "SSL_CONTEXT": httpclient_ssl_context,
    }
    app = create_app(TEST_CONFIG)

    with app.app_context():
        yield TestApp(app)


@pytest.fixture
def file_db_path(tmp_path):
    return f"sqlite:///{tmp_path}/db.sqlite"


@pytest.fixture
def file_db_engine(file_db_path):
    from models import db_engine

    return db_engine(file_db_path, init=True)


@pytest.fixture
def file_db_session(file_db_engine):
    from models import db_session

    return db_session(engine=file_db_engine)


@pytest.fixture
def file_db_testclient(testclient, file_db_path):
    from tasks import app

    testclient.app.config["DB_URI"] = file_db_path
    app.conf["DB_URI"] = file_db_path
    yield testclient


@pytest.fixture
def provider():
    """Sample Domain object configured with the fqdn 'example.com'."""
    from models.provider import Provider

    return Provider(domain="example.com")


@pytest.fixture(scope="session")
def dns_server_process(dnsserver_port):
    from dnserver import DNSServer

    server = DNSServer(port=dnsserver_port, upstream=None)
    server.start()
    yield server
    server.stop()


@pytest.fixture
def dns_server(request, dns_server_process):
    """Local DNS server.

    The DNS servers tries to load a zone file at
    'tests/fixtures/{test_name}.toml' if it exists, else
    'tests/fixtures/zones.toml'. Then it renders the file with jinja2 so
    test fixtures are available as variables inside the zones file. This
    allows to dynamically use port and hosts inside the zones file.
    """

    import tempfile
    from jinja2 import Template
    from dnserver.load_records import load_records

    test_name = request.node.name
    test_fixtures = {
        fixture_name: request.getfixturevalue(fixture_name)
        for fixture_name in request.fixturenames
        if fixture_name != "dns_server"
    }

    zones_filename = (
        test_name if os.path.exists(f"tests/fixtures/{test_name}.toml") else "zones"
    )

    with open(f"tests/fixtures/{zones_filename}.toml") as fd:
        template = Template(fd.read())
        zone = template.render(**test_fixtures)

    with tempfile.NamedTemporaryFile() as fd:
        fd.write(zone.encode())
        fd.flush()
        records = load_records(fd.name)
        dns_server_process.set_records(records.zones)

    yield dns_server_process


# TODO: maybe those fixtures are not needed anymore with httpdfix 1.0.7
@pytest.fixture(scope="session")
def make_httpserver_ipv4(httpserver_port):
    from pytest_httpserver.httpserver import HTTPServer

    server = HTTPServer(host="127.0.0.1", port=httpserver_port, ssl_context=None)
    server.start()
    yield server
    server.clear()
    if server.is_running():
        server.stop()


@pytest.fixture(scope="session")
def make_httpserver_ipv6(httpserver_port):
    from pytest_httpserver.httpserver import HTTPServer

    server = HTTPServer(host="::1", port=httpserver_port, ssl_context=None)
    server.start()
    yield server
    server.clear()
    if server.is_running():
        server.stop()


@pytest.fixture(scope="session")
def make_httpsserver_ipv4(httpserver_ssl_context, httpsserver_port):
    from pytest_httpserver.httpserver import HTTPServer

    server = HTTPServer(
        host="127.0.0.1", port=httpsserver_port, ssl_context=httpserver_ssl_context
    )
    server.start()
    yield server
    server.clear()
    if server.is_running():
        server.stop()


@pytest.fixture(scope="session")
def make_httpsserver_ipv6(httpserver_ssl_context, httpsserver_port):
    from pytest_httpserver.httpserver import HTTPServer

    server = HTTPServer(
        host="::1", port=httpsserver_port, ssl_context=httpserver_ssl_context
    )
    server.start()
    yield server
    server.clear()
    if server.is_running():
        server.stop()


@pytest.fixture
def httpserver_ipv4(make_httpserver_ipv4):
    """Local http server listenning exclusively on IPV4."""
    server = make_httpserver_ipv4
    yield server
    server.clear()


@pytest.fixture
def httpserver_ipv6(make_httpserver_ipv6):
    """Local http server listenning exclusively on IPV6."""
    server = make_httpserver_ipv6
    yield server
    server.clear()


@pytest.fixture
def httpsserver_ipv4(make_httpsserver_ipv4):
    """Local https server listenning exclusively on IPV4."""
    server = make_httpsserver_ipv4
    yield server
    server.clear()


@pytest.fixture
def httpsserver_ipv6(make_httpsserver_ipv6):
    """Local https server listenning exclusively on IPV6."""
    server = make_httpsserver_ipv6
    yield server
    server.clear()


@pytest.fixture(scope="session")
def smtp_server_ipv4(smtp_server_port):
    from smtpdfix import SMTPDFix

    with SMTPDFix("127.0.0.1", smtp_server_port) as smtpd:
        yield smtpd


@pytest.fixture(scope="session")
def smtp_server_ipv6(smtp_server_port):
    from smtpdfix import SMTPDFix

    with SMTPDFix("::1", smtp_server_port) as smtpd:
        yield smtpd
