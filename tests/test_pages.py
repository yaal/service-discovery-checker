from flask_babel import refresh


def test_translations(testclient):
    res = testclient.get("/").follow()
    res.mustcontain("Service discovery checker")

    res = testclient.get("/en")
    res.mustcontain("Service discovery checker")

    res = testclient.get("/fr")
    assert "Vérification de découverte de services", "Translations are not compiled, please run 'pybabel compile --directory web/translations'"

    res = testclient.get("/ru", status=404)

    testclient.app.config["LANGUAGE"] = "fr"
    refresh()
    res = testclient.get("/").follow()
    res.mustcontain("Vérification de découverte de services")
    del testclient.app.config["LANGUAGE"]


def test_implementations(testclient):
    testclient.get("/en/implementations")
