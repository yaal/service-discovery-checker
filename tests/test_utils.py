from web.utils import AuthenticationMethod
from web.utils import Code
from web.utils import DNSRecord
from web.utils import Header
from web.utils import Hyperlink


def test_hyperlink():
    hyperlink = Hyperlink("https://yaal.coop")
    assert str(hyperlink) == "https://yaal.coop"
    assert repr(hyperlink) == "<Hyperlink link=https://yaal.coop>"
    assert hyperlink.__html__() == '<a href="https://yaal.coop">yaal.coop</a>'


def test_code():
    code = Code("_caldavs._tcp.example.com")
    assert str(code) == "_caldavs._tcp.example.com"
    assert repr(code) == "<Code content=_caldavs._tcp.example.com>"
    assert code.__html__() == "<code>_caldavs._tcp.example.com</code>"


def test_dns_record():
    dns_record = DNSRecord("AAAA")
    assert str(dns_record) == "AAAA"
    assert repr(dns_record) == "<DNSRecord type=AAAA>"
    assert (
        dns_record.__html__()
        == '<a href="https://datatracker.ietf.org/doc/html/rfc3596">AAAA</a>'
    )


def test_header():
    header = Header("www-authenticate")
    assert str(header) == "www-authenticate"
    assert repr(header) == "<Header name=www-authenticate>"
    assert (
        header.__html__()
        == '<a href="https://developer.mozilla.org/docs/Web/HTTP/Headers/WWW-Authenticate">www-authenticate</a>'
    )


def test_authentication_method():
    authentication_method = AuthenticationMethod(["Bearer"])
    assert str(authentication_method) == "Bearer"
    assert repr(authentication_method) == "<AuthenticationMethod type=['Bearer']>"
    assert (
        authentication_method.__html__()
        == '<a href="https://datatracker.ietf.org/doc/html/rfc6750">Bearer</a>'
    )
