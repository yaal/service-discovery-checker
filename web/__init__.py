import hashlib
import os

import toml
from dotenv import dotenv_values
from flask import Flask
from flask import g

from .i18n import setup_babel

DEBUG_DB_FILENAME = "test.db"


def setup_config(app, config=None):
    if app.debug:  # pragma: no cover
        app.config.from_mapping(
            {
                "SECRET_KEY": hashlib.md5(__file__.encode("utf-8")).digest(),
                "DB_URI": f"sqlite+pysqlite:///{DEBUG_DB_FILENAME}",
            }
        )

    if config:
        app.config.from_mapping(config)
    elif "CONFIG" in os.environ:  # pragma: no cover
        app.config.from_mapping(toml.load(os.environ.get("CONFIG")))

    app.config.update(**dotenv_values(".env"))


def setup_blueprints(app):
    import web.public

    app.url_map.strict_slashes = False

    app.register_blueprint(web.public.bp, url_prefix="/<lang_code>")


def setup_database(app):
    from models import db_session

    init_db = app.config.get("TESTING") or (
        app.debug
        and not os.path.exists(DEBUG_DB_FILENAME)
        or os.path.getsize(DEBUG_DB_FILENAME) == 0
    )

    @app.before_request
    def before_request():
        g.db = db_session(app.config["DB_URI"], init=init_db)


def setup_celery(app):
    from tasks import app as celery

    celery.conf.update(app.config)
    if "CELERY_CONFIG" in app.config:
        celery.conf.update(app.config["CELERY_CONFIG"])
    return celery


def create_app(config=None):
    app = Flask(__name__)
    setup_config(app, config)
    setup_babel(app)
    setup_blueprints(app)
    setup_database(app)
    setup_celery(app)
    return app
