import gettext

import pycountry
from flask import abort
from flask import g
from flask import redirect
from flask import request
from flask import url_for
from flask_babel import Babel
from flask_babel import current_app
from flask_babel import get_locale

DEFAULT_LANGUAGE_CODE = "en"


babel = Babel()


def native_language_name_from_code(code):
    language = pycountry.languages.get(alpha_2=code[:2])
    if code == DEFAULT_LANGUAGE_CODE:
        return language.name

    translation = gettext.translation(
        "iso639-3", pycountry.LOCALES_DIR, languages=[code]
    )
    return translation.gettext(language.name)


def available_languages_codes():
    return [str(translation) for translation in babel.list_translations()]


def locale_selector():
    if current_app.config.get("LANGUAGE"):
        return current_app.config.get("LANGUAGE")

    if not g.get("lang_code"):
        g.lang_code = request.accept_languages.best_match(
            available_languages_codes(), DEFAULT_LANGUAGE_CODE
        )

    return g.lang_code


def setup_babel(app):
    babel.init_app(app, locale_selector=locale_selector)

    @app.url_defaults
    def add_language_code(endpoint, values):
        if g.get("lang_code"):
            values.setdefault("lang_code", g.lang_code)

    @app.url_value_preprocessor
    def pull_lang_code(endpoint, values):
        if values:
            g.lang_code = values.pop("lang_code")

    @app.before_request
    def before_request():
        if g.get("lang_code") not in available_languages_codes():
            adapter = app.url_map.bind("")
            try:
                lang_code = locale_selector()
                endpoint, args = adapter.match(
                    "/" + lang_code + request.full_path.rstrip("/ ?")
                )
                return redirect(url_for(endpoint, **args), 301)
            except Exception:
                abort(404)

        if (
            request.url_rule
            and "lang_code" in request.url_rule.defaults
            and request.url_rule.defaults["lang_code"]
            != request.full_path.split("/")[1]
        ):  # pragma: no cover
            abort(404)

    @app.context_processor
    def global_processor():
        return {
            "languages": {
                lang_code: native_language_name_from_code(lang_code)
                for lang_code in available_languages_codes()
            },
            "locale": get_locale(),
        }
