Extract the messages with:

```
pybabel extract --mapping-file web/translations/babel.cfg --output-file web/translations/messages.pot web checks models tasks
```

Add a new language with:

```
pybabel init --input-file web/translations/messages.pot --output-dir web/translations --locale <LANG>
```

Update the catalogs with:

```
pybabel update --input-file web/translations/messages.pot --output-dir web/translations
```

Compile the catalogs with:

```
pybabel compile --directory web/translations
```
