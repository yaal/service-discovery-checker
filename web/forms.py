import re

import wtforms
from flask_babel import lazy_gettext as _
from flask_wtf import FlaskForm


def validate_domain(form, field):
    if not re.match(
        r"""
        (?=^.{,253}$)          # max. length 253 chars
        (?!^.+\.\d+$)          # TLD is not fully numerical
        (?=^[^-.].+[^-.]$)     # doesn't start/end with '-' or '.'
        (?!^.+(\.-|-\.).+$)    # levels don't start/end with '-'
        (?:[a-z\d-]            # uses only allowed chars
        {1,63}(\.|$))          # max. level length 63 chars
        {2,127}                # max. 127 levels
    """,
        field.data,
        re.X | re.I,
    ):
        raise wtforms.ValidationError(_("This is not a valid domain"))


def domain_exists(form, field):
    import whois

    if not whois.query(field.data):
        raise wtforms.ValidationError(_("This domain is not registered"))


class CheckForm(FlaskForm):
    domain = wtforms.StringField(
        _("Domain"),
        validators=[wtforms.validators.DataRequired(), validate_domain, domain_exists],
        render_kw={"placeholder": "nubla.fr"},
    )
    submit = wtforms.SubmitField(_("Validate"))
