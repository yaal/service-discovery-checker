import datetime

from checks import group_results
from flask import Blueprint
from flask import g
from flask import redirect
from flask import render_template
from flask import request
from flask import url_for
from models.provider import Provider
from models.result import Result
from sqlalchemy import select
from tasks import run_checks

from .forms import CheckForm

bp = Blueprint("public", __name__)


@bp.route("/", methods=["GET", "POST"])
def index():
    form = CheckForm(request.form or None)

    if request.form and form.validate():
        return redirect(url_for("public.result", domain=form.domain.data))

    return render_template("index.html", form=form)


@bp.route("/result/<domain>", methods=["GET", "POST"])
def result(domain):
    form = CheckForm(request.form or None)

    if request.form and not form.validate():
        return render_template("result.html", form=form, domain=domain)

    provider = g.db.execute(
        select(Provider).filter(
            Provider.domain == domain,
            Provider.last_update
            > datetime.datetime.utcnow() - datetime.timedelta(minutes=15),
        )
    ).all()

    if not provider:
        run_checks.apply_async((domain,))
        results = []

    else:
        provider = provider[0][0]
        results = [
            result
            for (result,) in g.db.execute(
                select(Result).filter_by(provider_id=provider.id)
            ).fetchall()
        ]
        results = group_results(results)

    return render_template(
        "result.html", form=form, results=results, provider=provider, domain=domain
    )


@bp.route("/implementations")
def implementations():
    return render_template("implementations.html")
