from furl import furl


class HTMLValue:
    pass


class Hyperlink(HTMLValue):
    def __init__(self, path, label=""):
        self.path = furl(path)
        self.label = label

    def __repr__(self):
        return f"<Hyperlink link={self.__str__()}>"

    def __str__(self):
        return str(self.path)

    def __html__(self):
        path = self.path
        if not path.scheme:
            path.scheme = "https"
        label = self.label or self.path.host or str(self.path)
        return f'<a href="{path}">{label}</a>'

    def __eq__(self, other):
        return isinstance(other, Hyperlink) and (
            self.path,
            self.label,
        ) == (other.path, other.label)


class Code(HTMLValue):
    def __init__(self, content):
        self.content = content

    def __repr__(self):
        return f"<Code content={self.content}>"

    def __str__(self):
        return self.content

    def __html__(self):
        return f"<code>{self.content}</code>"

    def __eq__(self, other):
        return isinstance(other, Code) and self.content == other.content


class DNSRecord(HTMLValue):
    DNS_RECORDS_DOCS = {
        "A": "https://datatracker.ietf.org/doc/html/rfc1035",
        "AAAA": "https://datatracker.ietf.org/doc/html/rfc3596",
        "MX": "https://tools.ietf.org/html/rfc1035",
        "SRV": "https://tools.ietf.org/html/rfc2782",
        "TXT": "https://tools.ietf.org/html/rfc1035",
    }

    def __init__(self, record_type):
        self.record_type = record_type

    def __repr__(self):
        return f"<DNSRecord type={self.record_type}>"

    def __str__(self):
        return self.record_type

    def __html__(self):
        return f'<a href="{self.DNS_RECORDS_DOCS[self.record_type]}">{self.record_type}</a>'

    def __eq__(self, other):
        return self.record_type == other.record_type


class Header(HTMLValue):
    HTTP_HEADERS_DOCS = {
        "www-authenticate": "https://developer.mozilla.org/docs/Web/HTTP/Headers/WWW-Authenticate"
    }

    def __init__(self, header_name):
        self.header_name = header_name

    def __str__(self):
        return self.header_name

    def __repr__(self):
        return f"<Header name={self.header_name}>"

    def __html__(self):
        return f'<a href="{self.HTTP_HEADERS_DOCS[self.header_name]}">{self.header_name}</a>'


class AuthenticationMethod(HTMLValue):
    AUTHENTICATION_METHOD_DOCS = {
        "basic": "https://datatracker.ietf.org/doc/html/rfc7617",
        "bearer": "https://datatracker.ietf.org/doc/html/rfc6750",
        "digest": "https://datatracker.ietf.org/doc/html/rfc7616",
        "hoba": "https://datatracker.ietf.org/doc/html/rfc7486",
        "ntlm": "https://datatracker.ietf.org/doc/html/rfc4559",
        "vapid": "https://datatracker.ietf.org/doc/html/rfc8292",
        "scram": "https://datatracker.ietf.org/doc/html/rfc7804",
    }

    def __init__(self, authentication_methods):
        self.authentication_methods = authentication_methods

    def __repr__(self):
        return f"<AuthenticationMethod type={self.authentication_methods}>"

    def __str__(self):
        return ", ".join(self.authentication_methods)

    def __html__(self):
        return ", ".join(
            [
                f'<a href="{self.AUTHENTICATION_METHOD_DOCS[authentication_method.lower()]}">{authentication_method}</a>'
                for authentication_method in self.authentication_methods
            ]
        )

    def __eq__(self, other):
        return isinstance(other, AuthenticationMethod) and set(
            self.authentication_methods
        ) == set(other.authentication_methods)
