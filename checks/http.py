import enum
import errno
import socket
import ssl

import aiohttp
from flask_babel import lazy_gettext as _
from furl import furl
from models.result import Result
from models.result import Status
from web.utils import AuthenticationMethod
from web.utils import Code
from web.utils import Hyperlink


MESSAGES = {
    "http_connection": {
        "success": {
            "message": _(
                "{domain} is accessible using {http_protocol} with {ip_protocol}"
            ),
            "details": _(
                "The webserver accepts {http_protocol} connections using {ip_protocol}"
            ),
            "status": Status.SUCCESS,
        },
        "skip": {
            "message": _(
                "{domain} {ip_protocol} {http_protocol} connection could not be tested"
            ),
            "details": _(
                "We have to skip the test because we do not have an {ip_protocol} connection"
            ),
            "status": Status.SKIP,
        },
        "error": {
            "message": _(
                "{domain} is not accessible using {http_protocol} with {ip_protocol}"
            ),
            "details": _(
                "The webserver does not accept {http_protocol} connections using {ip_protocol}"
            ),
            "status": Status.ERROR,
        },
        "error_ssl": {
            "message": _(
                "{domain} is badly configured for {http_protocol} with {ip_protocol}"
            ),
            "details": _(
                "The webserver accepts connections on the {http_protocol} port, but there seems to be a configuration issue."
            ),
            "status": Status.ERROR,
        },
        "error_certificate_mismatch": {
            "message": _("The certificate {domain} is badly configured"),
            "details": _(
                "The webserver accepts connections on the {http_protocol} port for {ip_protocol}, but the certificate has been issued for another domain."
            ),
            "status": Status.ERROR,
        },
        "error_certificate_expired": {
            "message": _("The certificate for {domain} has expired"),
            "details": _(
                "The webserver accepts connections on the {http_protocol} port for {ip_protocol}, but the certificate have expired."
            ),
            "status": Status.ERROR,
        },
    },
    "http_auth": {
        "info": {
            "message": _("The available authentication methods are: {methods}"),
            "details": _("The response {header_name} header is {header_value}"),
            "status": Status.INFO,
        },
        "warning": {
            "message": _("No authentication method has been detected"),
            "details": _("No {header_name} header was present in the response"),
            "status": Status.WARNING,
        },
    },
}


class SSLErrorConstants(enum.IntEnum):
    CERTIFICATE_EXPIRED = 10
    CERTIFICATE_MISMATCH = 64


def aiohttp_connector(family=socket.AF_INET):
    from tasks import app

    resolver = aiohttp.resolver.AsyncResolver(
        nameservers=app.conf.get("DNS_SERVERS"),
        tcp_port=app.conf.get("DNS_PORT"),
        udp_port=app.conf.get("DNS_PORT"),
    )
    connector = aiohttp.TCPConnector(
        family=family, resolver=resolver, ssl=app.conf.get("SSL_CONTEXT")
    )

    return connector


async def check_http(provider, url, group="website", family=None):
    assert family in (socket.AF_INET6, socket.AF_INET)
    url = furl(url)

    results = []
    if family != socket.AF_INET6:
        ip_protocol = "IPV4"
    else:
        ip_protocol = "IPV6"

    result = Result(
        check_id="http_connection",
        group=group,
        provider_id=provider.id,
        data={
            "domain": Hyperlink(f"://{url.host}"),
            "http_protocol": url.scheme,
            "ip_protocol": ip_protocol,
        },
    )

    try:
        async with aiohttp.ClientSession(
            connector=aiohttp_connector(family)
        ) as session:
            async with session.get(url.url):
                result.message_id = "success"

    except ssl.CertificateError as exc:
        # The certificate does not match the domain
        if exc.certificate_error.verify_code == SSLErrorConstants.CERTIFICATE_MISMATCH:
            result.message_id = "error_certificate_mismatch"

        # The certificate has expired
        elif exc.certificate_error.verify_code == SSLErrorConstants.CERTIFICATE_EXPIRED:
            result.message_id = "error_certificate_expired"

    except aiohttp.client_exceptions.ClientConnectorSSLError:
        # Attempt to connect in https but the server serves http
        if url.scheme == "https":
            result.message_id = "error_ssl"

    except aiohttp.client_exceptions.ClientConnectorError as exc:
        # The client does not have the expected IP connection, cannot test
        if exc.os_error.errno == errno.ENETUNREACH:  # pragma: no cover
            result.message_id = "skip"

        # The server does not have the expected IP connection
        elif exc.os_error.errno == errno.ECONNREFUSED:
            result.message_id = "error"

    except aiohttp.client_exceptions.ClientOSError:
        # Attempt to connect in http but the server serves https
        result.message_id = "error_ssl"

    if result.message_id:
        results.append(result)

    return results


async def check_http_ipv4(provider, url, group="website"):
    return await check_http(
        provider=provider,
        url=url,
        group=group,
        family=socket.AF_INET,
    )


async def check_http_ipv6(provider, url, group="website"):
    return await check_http(
        provider=provider,
        url=url,
        group=group,
        family=socket.AF_INET6,
    )


async def check_http_authentication(provider, url, group="website"):
    results = []
    url = furl(url)

    async with aiohttp.ClientSession(connector=aiohttp_connector()) as session:
        async with session.get(url.url) as response:
            try:
                www_authentication = response.headers.getall("www-authenticate")
            except KeyError:
                results.append(
                    Result(
                        check_id="http_auth",
                        message_id="warning",
                        group=group,
                        provider_id=provider.id,
                        data={
                            "header_name": "www-authenticate",
                            "header_value": "",
                        },
                    )
                )
            else:
                methods = list(
                    set(
                        map(
                            lambda header: header.lower().split(" ")[0],
                            www_authentication,
                        )
                    )
                )
                results.append(
                    Result(
                        check_id="http_auth",
                        message_id="info",
                        group=group,
                        provider_id=provider.id,
                        data={
                            "methods": AuthenticationMethod(methods),
                            "header_name": "www-authenticate",
                            "header_value": Code(", ".join(www_authentication)),
                        },
                    )
                )

    return results
