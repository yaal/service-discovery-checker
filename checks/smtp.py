import asyncio
import ssl

import aiodns
import aiosmtplib
from checks.ip import check_ipv4
from checks.ip import check_ipv6
from flask_babel import lazy_gettext as _
from models.result import Result
from models.result import Status
from web.utils import Code
from web.utils import DNSRecord
from web.utils import Hyperlink

MESSAGES = {
    "smtp_dns_records": {
        "success": {
            "message": _("The SMTP submission domain is {host}"),
            "details": _(
                "A {srv_record} {record_type} DNS record has been found for SMTP submission for {host} on port {port}"
            ),
            "status": Status.SUCCESS,
        },
        "error": {
            "message": _("No SMTP submission domain could be found"),
            "details": _(
                "No {srv_record} {record_type} DNS record has been found for SMTP submission"
            ),
            "status": Status.ERROR,
        },
    },
    "smtp_connection": {
        "success": {
            "message": _("The SMTP server is up on {host}"),
            "details": _("The SMTP server is listening on {host}:{port}"),
            "status": Status.SUCCESS,
        },
        "error": {
            "message": _("The SMTP server could not be reached"),
            "details": _("No SMTP server could be reached on {host}:{port}"),
            "status": Status.ERROR,
        },
    },
}


async def check_smtp(provider, domain, group="smtp"):
    results = await check_smtp_dns(provider, domain, group)
    if results[0].message_id != "success":
        return results

    smtp_domain = results[0].data["host"]
    ipv4_results, ipv6_results = await asyncio.gather(
        check_ipv4(provider, smtp_domain, group),
        check_ipv6(provider, smtp_domain, group),
    )
    results.extend(ipv4_results)
    results.extend(ipv6_results)

    if ipv4_results[0].message_id == "success":
        results.extend(
            await check_smtp_connection(
                provider, ipv4_results[0].data["ip"], results[0].data["port"], group
            )
        )

    if ipv6_results[0].message_id == "success":
        results.extend(
            await check_smtp_connection(
                provider, ipv6_results[0].data["ip"], results[0].data["port"], group
            )
        )

    return results


async def check_smtp_dns(provider, domain, group="smtp"):
    from tasks import app

    resolver = aiodns.DNSResolver(
        servers=app.conf.get("DNS_SERVERS"),
        tcp_port=app.conf.get("DNS_PORT"),
        udp_port=app.conf.get("DNS_PORT"),
    )
    results = []
    try:
        submission_srv = await resolver.query(f"_submission._tcp.{domain}", "SRV")
    except aiodns.error.DNSError:
        submission_srv = None

    if not submission_srv:
        results.append(
            Result(
                check_id="smtp_dns_records",
                message_id="error",
                group=group,
                provider_id=provider.id,
                data={
                    "domain": Hyperlink(domain),
                    "record_type": DNSRecord("SRV"),
                    "srv_record": Code(f"_submission._tcp.{domain}"),
                },
            )
        )
        return results
    else:
        results.append(
            Result(
                check_id="smtp_dns_records",
                message_id="success",
                group=group,
                provider_id=provider.id,
                data={
                    "domain": Hyperlink(domain),
                    "record_type": DNSRecord("SRV"),
                    "srv_record": Code(f"_submission._tcp.{domain}"),
                    "host": submission_srv[0].host,
                    "port": submission_srv[0].port,
                },
            )
        )
    return results


async def check_smtp_connection(provider, host, port, group="smtp"):
    results = []
    smtp_client = aiosmtplib.SMTP(hostname=host, port=port, timeout=60)
    try:
        smtp_connection = await smtp_client.connect()
        await smtp_client.quit()
    except (aiosmtplib.errors.SMTPConnectError, ssl.SSLError):
        smtp_connection = None

    host = f"[{host}]" if ":" in host else host
    if not smtp_connection:
        results.append(
            Result(
                check_id="smtp_connection",
                message_id="error",
                group=group,
                provider_id=provider.id,
                data={
                    "host": host,
                    "port": port,
                },
            )
        )
    else:
        results.append(
            Result(
                check_id="smtp_connection",
                message_id="success",
                group=group,
                provider_id=provider.id,
                data={
                    "host": host,
                    "port": port,
                },
            )
        )

    return results
