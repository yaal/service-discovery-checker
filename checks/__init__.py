def group_results(*args):
    grouped_results = {}
    for results in args:
        for result in results:
            grouped_results.setdefault(
                result.group, {"status": "success", "results": []}
            )["results"].append(result)

            if grouped_results[result.group]["status"] in (
                "success",
                "warning",
            ) and result.status.value in ("error", "warning"):
                grouped_results[result.group]["status"] = result.status.value

    return grouped_results
