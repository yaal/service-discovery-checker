import asyncio

import aiodns
from flask_babel import lazy_gettext as _
from furl import furl
from models.result import Result
from models.result import Status
from web.utils import Code
from web.utils import DNSRecord
from web.utils import Hyperlink

from .http import check_http_authentication
from .http import check_http_ipv4
from .http import check_http_ipv6
from .ip import check_ipv4
from .ip import check_ipv6


MESSAGES = {
    "caldav_dns_records": {
        "success": {
            "status": Status.SUCCESS,
            "message": _("The CalDAVs endpoint is {url}"),
            "details": _(
                "The domain has "
                "a {caldavs_srv_record_name} {srv_record} DNS record which value is {caldavs_srv_record_value} and "
                "a {caldavs_txt_record_name} {txt_record} DNS record which value is {caldavs_txt_record_value}."
            ),
        },
        "error": {
            "status": Status.ERROR,
            "message": _("No CalDAVs nor CalDAV DNS record"),
            "details": _(
                "No DNS {srv_record} record has been found for "
                "neither {caldavs_srv_record_name} nor {caldavs_srv_record_name}."
            ),
        },
        "caldav_only": {
            "status": Status.WARNING,
            "message": _(
                "The CalDAV endpoint is {url}, but the connection is not secured"
            ),
            "details": _(
                "The domain has "
                "a {caldav_srv_record_name} {srv_record} DNS record which value is {caldav_srv_record_value} and "
                "a {caldav_txt_record_name} {txt_record} DNS record which value is {caldav_txt_record_value}. "
                "However a {caldav_srv_record_name} {srv_record} DNS record, "
                "and a {caldav_txt_record_name} {txt_record} DNS record "
                "are missing to make the connection secured."
            ),
        },
        "caldavs_domain_but_no_endpoint": {
            "status": Status.WARNING,
            "message": _(
                "The CalDAVs domain is {caldavs_domain}, but no endpoint is defined"
            ),
            "details": _(
                "The domain has a {caldavs_srv_record_name} {srv_record} DNS record "
                "wihch value is {caldavs_srv_record_value}, "
                "but there is no {caldavs_txt_record_value} {txt_record} DNS record registered. "
                "The CalDAVs endpoint might be {url}."
            ),
        },
        "caldav_domain_but_no_endpoint": {
            "status": Status.WARNING,
            "message": _("The CalDAV endoint is {endpoint}, but no domain is defined"),
            "details": _(
                "The domain has a {caldav_srv_record_name} {srv_record} DNS record "
                "wihch value is {caldav_srv_record_value}, "
                "but there is no {caldav_txt_record_value} {txt_record} DNS record registered. "
                "The CalDAVs endpoint might be {url}."
            ),
        },
    },
}


async def check_caldav(provider, domain, group="dav"):
    records_results = await check_caldav_dns_records(provider, domain, group)
    results_lists = [records_results]

    if records_results[0].message_id == "success":
        url = str(records_results[0].data["url"])
        results_lists.extend(
            await asyncio.gather(*check_caldav_http_endpoint(provider, url, group))
        )

    elif records_results[0].message_id == "caldav_only":
        url = str(records_results[0].data["url"])
        results_lists.extend(
            await asyncio.gather(*check_caldav_http_endpoint(provider, url, group))
        )

    results = []
    for result_list in results_lists:
        results.extend(result_list)
    return results


def srv_to_str(record):
    return f"{record.priority} {record.weight} {record.port} {record.host}"


def txt_to_str(record):
    return f"{record.text}"


async def check_caldav_dns_records(provider, domain, group="dav"):
    from tasks import app

    resolver = aiodns.DNSResolver(
        servers=app.conf.get("DNS_SERVERS"),
        tcp_port=app.conf.get("DNS_PORT"),
        udp_port=app.conf.get("DNS_PORT"),
    )

    caldavs_srv, caldav_srv, caldavs_txt, caldav_txt = await asyncio.gather(
        resolver.query(f"_caldavs._tcp.{domain}", "SRV"),
        resolver.query(f"_caldav._tcp.{domain}", "SRV"),
        resolver.query(f"_caldavs._tcp.{domain}", "TXT"),
        resolver.query(f"_caldav._tcp.{domain}", "TXT"),
        return_exceptions=True,
    )

    caldavs_domain = (
        caldavs_srv[0].host if not isinstance(caldavs_srv, Exception) else None
    )
    caldav_domain = (
        caldav_srv[0].host if not isinstance(caldav_srv, Exception) else None
    )
    caldavs_port = (
        caldavs_srv[0].port if not isinstance(caldavs_srv, Exception) else None
    )
    caldav_port = caldav_srv[0].port if not isinstance(caldav_srv, Exception) else None

    caldavs_endpoint = (
        caldavs_txt[0].text[len("path=") :]
        if not isinstance(caldavs_txt, Exception)
        else None
    )
    caldav_endpoint = (
        caldav_txt[0].text[len("path=") :]
        if not isinstance(caldav_txt, Exception)
        else None
    )
    caldavs_url = furl(
        scheme="https", host=caldavs_domain, port=caldavs_port, path=caldavs_endpoint
    )
    caldav_url = furl(
        scheme="http", host=caldav_domain, port=caldav_port, path=caldav_endpoint
    )

    result_data_base = {
        "srv_record": DNSRecord("SRV"),
        "txt_record": DNSRecord("TXT"),
        "caldavs_srv_record_name": Code(f"_caldavs._tcp.{domain}"),
        "caldavs_txt_record_name": Code(f"_caldavs._tcp.{domain}"),
        "caldav_srv_record_name": Code(f"_caldav._tcp.{domain}"),
        "caldav_txt_record_name": Code(f"_caldav._tcp.{domain}"),
    }

    results = []
    if caldavs_domain:
        if caldavs_endpoint:
            results.append(
                Result(
                    check_id="caldav_dns_records",
                    message_id="success",
                    group=group,
                    provider_id=provider.id,
                    data={
                        "url": Hyperlink(caldavs_url),
                        "caldavs_srv_record_value": Code(srv_to_str(caldavs_srv[0])),
                        "caldavs_txt_record_value": Code(txt_to_str(caldavs_txt[0])),
                        **result_data_base,
                    },
                )
            )
        else:
            results.append(
                Result(
                    check_id="caldav_dns_records",
                    message_id="caldavs_domain_but_no_endpoint",
                    group=group,
                    provider_id=provider.id,
                    data={
                        "url": Hyperlink(caldavs_url),
                        "caldavs_srv_record_value": Code(srv_to_str(caldavs_srv[0])),
                        **result_data_base,
                    },
                )
            )

    elif caldav_domain:
        if caldav_endpoint:
            results.append(
                Result(
                    check_id="caldav_dns_records",
                    message_id="caldav_only",
                    group=group,
                    provider_id=provider.id,
                    data={
                        "url": Hyperlink(caldav_url),
                        "caldav_srv_record_value": Code(srv_to_str(caldav_srv[0])),
                        "caldav_txt_record_value": Code(txt_to_str(caldav_txt[0])),
                        **result_data_base,
                    },
                )
            )
        else:
            results.append(
                Result(
                    check_id="caldav_dns_records",
                    message_id="caldav_domain_but_no_endpoint",
                    group=group,
                    provider_id=provider.id,
                    data={
                        "url": Hyperlink(caldav_url),
                        "caldav_srv_record_value": Code(srv_to_str(caldav_srv[0])),
                        **result_data_base,
                    },
                )
            )

    else:
        results.append(
            Result(
                check_id="caldav_dns_records",
                message_id="error",
                group=group,
                provider_id=provider.id,
                data=result_data_base,
            )
        )

    return results


def check_caldav_http_endpoint(provider, url, group="dav"):
    url = furl(url)
    return [
        check_ipv6(provider, url.host, group=group),
        check_ipv4(provider, url.host, group=group),
        check_http_ipv6(provider, url, group=group),
        check_http_ipv4(provider, url, group=group),
        check_http_authentication(provider, url, group=group),
    ]
