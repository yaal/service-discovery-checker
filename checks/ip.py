import aiodns
from flask_babel import lazy_gettext as _
from models.result import Result
from models.result import Status
from web.utils import DNSRecord
from web.utils import Hyperlink

MESSAGES = {
    "ip": {
        "success": {
            "message": _("{domain} domain is configured for {protocol}"),
            "details": _("A {record_type} DNS record has been found pointing to {ip}"),
            "status": Status.SUCCESS,
        },
        "error": {
            "message": _("{domain} domain is not configured for {protocol}"),
            "details": _("No {record_type} DNS record has been found"),
            "status": Status.ERROR,
        },
    },
}


async def check_ipv4(provider, domain, group="website"):
    return await check_ip_record(provider, domain, "ipv4", "A", group)


async def check_ipv6(provider, domain, group="website"):
    return await check_ip_record(provider, domain, "ipv6", "AAAA", group)


async def check_ip_record(provider, domain, protocol, record_type, group="website"):
    from tasks import app

    resolver = aiodns.DNSResolver(
        servers=app.conf.get("DNS_SERVERS"),
        tcp_port=app.conf.get("DNS_PORT"),
        udp_port=app.conf.get("DNS_PORT"),
    )
    results = []
    try:
        ip = await resolver.query(domain, record_type)
        results.append(
            Result(
                check_id="ip",
                message_id="success",
                group=group,
                provider_id=provider.id,
                data={
                    "domain": Hyperlink(f"://{domain}"),
                    "protocol": protocol,
                    "record_type": DNSRecord(record_type),
                    "ip": ip[0].host,
                },
            )
        )
    except (aiodns.error.DNSError, IndexError):
        results.append(
            Result(
                check_id="ip",
                message_id="error",
                group=group,
                provider_id=provider.id,
                data={
                    "domain": Hyperlink(f"://{domain}"),
                    "protocol": protocol,
                    "record_type": DNSRecord(record_type),
                },
            )
        )

    return results
