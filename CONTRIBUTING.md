The project is based on flask for the web part, and celery for the tasks part.
As the tasks are mostly I/O bound operations, we prefer to use async libs to perform them (aiodns, aiohttp etc.)

We use [poetry](https://python-poetry.org) as our main packaging tool.
If it is not already present (or available) on your system, please follow the [installation instructions](https://python-poetry.org/docs/#installation).

Then, install the project dependencies:

```
poetry install
```

Launch the webserver and the celery worker with honcho:

```
poetry run honcho start
```

The default celery broken is sqlite, but you can configure your own.
The configuration can be written as environment variables inside a `.env` file, and will be loaded automatically by celery and Flask.
Please have a look on the celery documentation to know how to configure your broker:
https://docs.celeryq.dev/en/stable/userguide/configuration.html
