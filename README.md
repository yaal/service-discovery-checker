# service-discovery-checker

A tool to check how your provider services are automagically discoverable, and other good-practices.

DNS have [SRV records](https://en.wikipedia.org/wiki/SRV_record) that help to find the right subdomain for a given service,
and [TXT records](https://en.wikipedia.org/wiki/TXT_record) that are sometimes used to help find the right endpoint for a
given service. Several services specifications define which entries must be configured so services are discoverable:
